import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native';
import Ico7Img from '../../assets/ico7.png';
import Ico8Img from '../../assets/ico8.png';
import Photo5Img from '../../assets/img5.png';
import Ico10Img from '../../assets/ico10.png';
import Ico9Img from '../../assets/ico9.png';

const Article = ({ navigation }) => {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => <Image style={styles.ico8Img} source={Ico8Img} />,
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
          <Image style={styles.ico7Img} source={Ico7Img} />
        </TouchableOpacity>
      ),
      headerTitle: () => false,
      headerTransparent: () => true,
    });
  }, [navigation]);
  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContainer}
        >
          <Image style={styles.photo5Img} source={Photo5Img} />
          <Image style={styles.ico10Img} source={Ico10Img} />
          <View style={styles.label}>
            <Text style={styles.text}>Blueberry Cake</Text>
            <Text style={styles.rating}>
              <Image style={styles.ico9Img} source={Ico9Img} />
              5.0
            </Text>
          </View>
          <Text style={styles.description}>
            Blue berry cake with fresh cream taste and fresh ingrediants of
            Blueberry. We have wide range of cakes on our store. Also we can
            order for events.
          </Text>
          <View style={styles.cards}>
            <View style={styles.card}>
              <Text style={styles.line1}>Delivery Time</Text>
              <Text style={styles.line2}>45 Mins</Text>
            </View>
            <View style={styles.card2}>
              <Text style={styles.line1}>Total Price</Text>
              <Text style={styles.line2}>$65.00</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ico8Img: {
    display: 'flex',
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 28,
    marginRight: 28,
  },
  ico7Img: {
    display: 'flex',
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 28,
    marginRight: 28,
  },
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#F5F5F5',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  main: {
    display: 'flex',
    width: '100%',
    paddingLeft: 28,
    paddingRight: 28,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photo5Img: {
    display: 'flex',
    marginTop: 64,
    width: 358,
    height: 451,
  },
  ico10Img: {
    display: 'flex',
    width: 57,
    height: 7,
    marginTop: 12,
  },
  ico9Img: {
    display: 'flex',
    width: 23,
    height: 22,
    marginRight: 4,
  },
  label: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginTop: 24,
  },
  text: {
    fontSize: 32,
    fontWeight: '600',
    color: '#262626',
  },
  rating: {
    display: 'flex',
    flexDirection: 'row',
    fontSize: 22,
    fontWeight: '600',
    alignItems: 'center',
    color: '#262626',
  },
  description: {
    display: 'flex',
    fontSize: 16,
    lineHeight: 20,
    marginTop: 12,
    color: '#727272',
  },
  cards: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    marginTop: 24,
    marginBottom: 40,
  },
  card: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 12,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    minWidth: 180,
  },
  card2: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 12,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginLeft: 12,
  },
  line1: {
    fontSize: 16,
    color: '#727272',
  },
  line2: {
    fontSize: 18,
    fontWeight: '600',
    color: '#262626',
  },
  scrollView: {
    display: 'flex',
    overflow: 'scroll',
    height: 'auto',
    maxHeight: Dimensions.get('window').height,
  },
  scrollViewContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    // justifyContent: 'space-between',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Article;
