import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Ico1Img from '../../assets/ico1.png';
import Ico2Img from '../../assets/ico2.png';
import Ico3Img from '../../assets/ico3.png';
import PointsImg from '../../assets/ico4.png';
import CartImg from '../../assets/ico5.png';
import HeartImg from '../../assets/ico6.png';
import Item1Img from '../../assets/img1.png';
import Item2Img from '../../assets/img2.png';
import Item3Img from '../../assets/img3.png';
import Item4Img from '../../assets/img4.png';
import Constants from 'expo-constants';
console.log(Constants);

const Dashboard = ({ navigation }) => {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => <Image style={styles.cartIcon} source={CartImg} />,
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.navigate('Splash')}>
          <Image style={styles.retourIcon} source={PointsImg} />
        </TouchableOpacity>
      ),
      headerTitle: () => false,
      headerTransparent: () => true,
    });
  }, [navigation]);
  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <View style={styles.header}>
          <Text style={styles.title}>Fresh Taste of</Text>
          <Text style={styles.subtitle}>Designer Cakes</Text>
          <View style={styles.actions}>
            <View style={styles.actionBtn}>
              <Image style={styles.Ico1Img} source={Ico1Img} />
            </View>
            <View style={styles.actionBtn}>
              <Image style={styles.Ico2Img} source={Ico2Img} />
            </View>
            <View style={styles.actionBtn}>
              <Image style={styles.Ico3Img} source={Ico3Img} />
            </View>
            <View style={styles.actionBtnGreen}>
              <Text style={styles.Ico4Text}>All</Text>
            </View>
          </View>
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContainer}
          >
            <View style={styles.scrollViewCol}>
              <View
                style={styles.card}
                onStartShouldSetResponder={() => true}
                onResponderGrant={() => navigation.navigate('Article')}
              >
                <Image
                  style={(styles.ItemImg, styles.Item1Img)}
                  source={Item1Img}
                />
                <Text style={styles.cardTitle}>Cold Coffee</Text>
                <Text style={styles.cardSubtitle}>Lime with Coffee</Text>
                <View style={styles.footer}>
                  <Text style={styles.solde}>$24.00</Text>
                  <Image style={styles.likeIcon} source={HeartImg} />
                </View>
              </View>
              <View style={styles.card}>
                <Image
                  style={(styles.ItemImg, styles.Item3Img)}
                  source={Item3Img}
                />
                <Text style={styles.cardTitle}>Strawberry Cake</Text>
                <Text style={styles.cardSubtitle}>Cream with Stawberry</Text>
                <View style={styles.footer}>
                  <Text style={styles.solde}>$12.00</Text>
                  <Image style={styles.likeIcon} source={HeartImg} />
                </View>
              </View>
            </View>
            <View style={styles.scrollViewCol}>
              <View style={styles.card}>
                <Image
                  style={(styles.ItemImg, styles.Item2Img)}
                  source={Item2Img}
                />
                <Text style={styles.cardTitle}>Blueberry Cake</Text>
                <Text style={styles.cardSubtitle}>Cream with Berry</Text>
                <View style={styles.footer}>
                  <Text style={styles.solde}>$65.00</Text>
                  <Image style={styles.likeIcon} source={HeartImg} />
                </View>
              </View>
              <View style={styles.card}>
                <Image
                  style={(styles.ItemImg, styles.Item4Img)}
                  source={Item4Img}
                />
                <Text style={styles.cardTitle}>Hot Coffee</Text>
                <Text style={styles.cardSubtitle}>Fresh Coffee</Text>
                <View style={styles.footer}>
                  <Text style={styles.solde}>$30.00</Text>
                  <Image style={styles.likeIcon} source={HeartImg} />
                </View>
              </View>
              <View style={styles.cardViewMore}>
                <Text style={styles.showMoreBtn}>View More</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#F5F5F5',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  cartIcon: {
    display: 'flex',
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 28,
    marginRight: 28,
  },
  retourIcon: {
    display: 'flex',
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 28,
    marginRight: 28,
  },
  likeIcon: {
    display: 'flex',
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  main: {
    display: 'flex',
    width: '100%',
    // boxSizing: 'border-box',
    paddingLeft: 28,
    paddingRight: 28,
  },
  header: {
    display: 'flex',
    justifyContent: 'flex-start',
    marginTop: 100,
  },
  title: {
    display: 'flex',
    fontSize: 34,
    fontWeight: '500',
    color: '#262626',
    lineHeight: 34,
  },
  subtitle: {
    display: 'flex',
    fontSize: 38,
    lineHeight: 38,
    fontWeight: '200',
    color: '#727272',
  },
  scrollView: {
    marginTop: 28,
    display: 'flex',
    overflow: 'scroll',
    height: 'auto',
    maxHeight: Dimensions.get('window').height - 278,
  },
  scrollViewContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',

    // width: '116%',
    // marginLeft: -28,
    // paddingLeft: 28,
    // paddingRight: 28,

    // --------------------
    // display: 'grid',
    // gridAutoRows: 'min-content',
    // gridGap: 12,
    // gridTemplateColumns: 'repeat(auto-fill, minmax(40%, 1fr))',
  },
  scrollViewCol: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    // boxSizing: 'border-box',
    // width: 'calc(50% - 8px)',
    width: '48%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 12,
    padding: 12,
    // boxSizing: 'border-box',
    alignSelf: 'baseline',
    // gridRow: 'span 1',
    width: '100%',
    marginBottom: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',

    // shadowColor: '#ced5da',
    // shadowOffset: {
    //   width: 0,
    //   height: 7,
    // },
    // shadowOpacity: 0.41,
    // shadowRadius: 9.11,

    // elevation: 8,
  },
  ItemImg: {
    display: 'flex',
    resizeMode: 'contain',
  },
  Item1Img: {
    width: 148,
    height: 148,
  },
  Item2Img: {
    width: 126,
    height: 158,
  },
  Item3Img: {
    width: 161,
    height: 110,
  },
  Item4Img: {
    width: 150,
    height: 144,
  },
  cardTitle: {
    display: 'flex',
    fontSize: 16,
    fontWeight: '600',
    color: '#262626',
    width: '100%',
    marginTop: 12,
  },
  cardSubtitle: {
    display: 'flex',
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '200',
    color: '#727272',
    width: '100%',
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: 4,
  },
  solde: {
    display: 'flex',
    fontSize: 14,
    fontWeight: '500',
    color: '#262626',
  },
  showMoreBtn: {
    backgroundColor: '#3D946A',
    color: '#fff',
    fontSize: 16,
    fontWeight: '400',
  },

  cardViewMore: {
    backgroundColor: '#3D946A',
    borderRadius: 12,
    padding: 12,
    alignSelf: 'baseline',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 40,
  },
  actions: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16,
  },
  actionBtn: {
    backgroundColor: '#fff',
    borderRadius: 50,
    padding: 12,
    width: 60,
    height: 60,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionBtnGreen: {
    backgroundColor: '#3D946A',
    borderRadius: 50,
    padding: 12,
    width: 60,
    height: 60,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Ico1Img: {
    width: 36,
    height: 39,
  },
  Ico2Img: {
    width: 41,
    height: 40,
  },
  Ico3Img: {
    width: 30,
    height: 37,
  },
  Ico4Text: {
    color: '#fff',
    fontSize: 18,
  },
});

export default Dashboard;
