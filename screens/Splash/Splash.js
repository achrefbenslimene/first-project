import React from 'react';
// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';
import BackgroundImg from '../../assets/bg.png';
import CakeImg from '../../assets/img.png';

const Splash = ({ navigation }) => {
  return (
    <View
      style={styles.container}
      onStartShouldSetResponder={() => true}
      onResponderGrant={() => navigation.navigate('Dashboard')}
    >
      <ImageBackground source={BackgroundImg} style={styles.image}>
        <View style={styles.content}>
          <View style={styles.textes}>
            <Text style={styles.title}>Designer</Text>
            <Text style={styles.title}>Cakes Delivered</Text>
            <Text style={styles.subtitle}>To your doorstep</Text>
          </View>
          <Image style={styles.cakeImg} source={CakeImg} />
        </View>
      </ImageBackground>
      {/* <StatusBar style="auto" /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  touch: {
    display: 'flex',
    width: '100%',
    height: '100%',
    zIndex: 10,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textes: {
    display: 'flex',
    justifyContent: 'flex-start',
  },
  image: {
    flex: 1,
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cakeImg: {
    display: 'flex',
    width: 318,
    height: 216,
    resizeMode: 'contain',
    marginTop: 32,
  },
  title: {
    display: 'flex',
    fontSize: 42,
    fontWeight: '500',
    color: '#fff',
    lineHeight: 42,
  },
  subtitle: {
    display: 'flex',
    fontSize: 32,
    fontWeight: '400',
    color: '#fff',
  },
});

export default Splash;
